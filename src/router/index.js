import Vue from 'vue'
import Router from 'vue-router'

//Fornecedor
import Fornecedores from '@/components/Fornecedores/Fornecedores'
import AddFornecedor from '@/components/Fornecedores/AddFornecedor'
import EditFornecedor from '@/components/Fornecedores/EditFornecedor'
import ViewFornecedor from '@/components/Fornecedores/ViewFornecedor'

//Produto
import Produtos from '@/components/Produtos/Produtos'
import AddProduto from '@/components/Produtos/AddProduto'
import EditProduto from '@/components/Produtos/EditProduto'
import ViewProduto from '@/components/Produtos/ViewProduto'

//Estoque
import Estoque from '@/components/Estoque/Estoque'
import ViewItemEstoque from '@/components/Estoque/ViewItemEstoque'

//Movimentacao

import Movimentacao from '@/components/Movimentacao/Movimentacao'
import AddMovimentacao from '@/components/Movimentacao/AddMovimentacao'
import ViewMovimentacao from '@/components/Movimentacao/ViewMovimentacao'


Vue.component('ViewFornecedor', ViewFornecedor)
Vue.component('AddFornecedor', AddFornecedor)
Vue.component('EditFornecedor', EditFornecedor)

Vue.component('ViewProduto', ViewProduto)
Vue.component('AddProduto', AddProduto)
Vue.component('EditProduto', EditProduto)

Vue.component('ViewItemEstoque', ViewItemEstoque)

Vue.component('AddMovimentacao', AddMovimentacao)
Vue.component('ViewMovimentacao', ViewMovimentacao)

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', redirect: '/fornecedores' },

    //Fornecedor
    {
      path: '/fornecedores',
      name: 'fornecedores',
      component: Fornecedores
    },

    {
      path: '/addFornecedor',
      name: 'AddFornecedor',
      component: AddFornecedor
	   }
    ,
    {
      path: '/editFornecedor',
      name: 'EditFornecedor',
      component: EditFornecedor
     }
    ,
    {
      path: '/viewFornecedor',
      name: 'ViewFornecedor',
      component: ViewFornecedor
    },

    //Produto
    {
      path: '/produtos',
      name: 'Produtos',
      component: Produtos
    },

    {
      path: '/AddProduto',
      name: 'AddProduto',
      component: AddProduto
    },

    {
      path: '/EditProduto',
      name: 'EditProduto',
      component: EditProduto
    },

    {
      path: '/ViewProduto',
      name: 'ViewProduto',
      component: ViewProduto
    },


    //Movimentacao
    {
      path: '/Movimentacao',
      name: 'Movimentacao',
      component: Movimentacao
    },

    {
      path: '/AddMovimentacao',
      name: 'AddMovimentacao',
      component: AddMovimentacao
    },


    //Estoque
    {
      path: '/Estoque',
      name: 'Estoque',
      component: Estoque
    },

    {
      path: '/ViewItemEstoque',
      name: 'ViewItemEstoque',
      component: ViewItemEstoque
    },

    { path: '*', redirect: '/fornecedores' }
  ]
})
